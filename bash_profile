# ~/.bash_profile
# 
# Many things taken from: https://github.com/mathiasbynens/dotfiles/

# Load the shell dotfiles:
source ~/.colors
source ~/.aliases
source ~/.extras

# Terminal prompt
export PS1="$COLOR_RED\h$COLOR_NC|\W: "

# Search /usr/local/bin before the rest of the PATH
PATH=/usr/local/bin:$PATH

# Make homebrew version of GCC the default:
export CC=/usr/local/bin/gcc-4.8

# Better bash history
export HISTFILESIZE=100000
export HISTSIZE=100000
export HISTCONTROL=ignoredups
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help"

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
# This is kinda cool but also kinda slow
# [ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2 | tr ' ' '\n')" scp sftp ssh;

# Add tab completion for many Bash commands
# This is kinda cool but also kinda slow at startup
# if which brew > /dev/null && [ -f "$(brew --prefix)/etc/bash_completion" ]; then
# 	source "$(brew --prefix)/etc/bash_completion";
# elif [ -f /etc/bash_completion ]; then
# 	source /etc/bash_completion;
# fi;

# Make vim the default editor
export EDITOR="vim";

# Highlight section titles in manual pages
export LESS_TERMCAP_md="${yellow}";

# Added by Canopy installer on 2014-10-20
# VIRTUAL_ENV_DISABLE_PROMPT can be set to '' to make bashprompt show that Canopy is active, otherwise 1
VIRTUAL_ENV_DISABLE_PROMPT=1 source /usr/local/Canopy/User/bin/activate
