# This file is intended to be read and executed when a python interperter is 
# started.  There are two ways of doing this:
# 
# 1) Store the file wherever you want (e.g. $HOME/python-startup.py) and assign
#    the PYTHONSTARTUP environment variable to this its location
# 2) Store this file in the site-packages directory for your python 
#    installation.  It can be named either usercustomize.py or sitecustomize.py.
#    To find this directory, get the base directory with:
#        >>> import site
#        >>> print(site.PREFIXES[0])
#    Then look for something like:
#        /whatever-that-outputs/lib/python[version]/site-packages
# 
# The advantage of the second method is that it will work even if you run a code
# interactively.  For example, if you run "python -i my_script.py" with this 
# setup the first way, you will not have tab completion or the other awesome 
# features that you have here.  If you have it setup the second way, you will 
# have these features.

#=== Tab completion ============================================================
import readline
import rlcompleter
readline.parse_and_bind("tab: complete")
del readline, rlcompleter
#=== Add directories to your python path =======================================
import sys
sys.path.append('/usr/local/JWG/bin')
del sys
#=== Improved history ==========================================================
import atexit
import os
import readline
historyPath = os.path.expanduser("~/.python_history")
def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

if os.path.exists(historyPath):
    readline.read_history_file(historyPath)

atexit.register(save_history)

home_dir = os.path.expanduser('~/')
del os, atexit, save_history, historyPath
#===============================================================================

