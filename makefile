install:
	ln -s $(PWD)/aliases $(HOME)/.aliases
	ln -s $(PWD)/bash_profile $(HOME)/.bash_profile
	ln -s $(PWD)/colors $(HOME)/.colors
	ln -s $(PWD)/gitconfig $(HOME)/.gitconfig
	ln -s $(PWD)/htoprc $(HOME)/.htoprc
	ln -s $(PWD)/inputrc $(HOME)/.inputrc
	ln -s $(PWD)/screenrc $(HOME)/.screenrc
	ln -s $(PWD)/vimrc $(HOME)/.vimrc

