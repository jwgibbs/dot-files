Overview
========

These are some .[...]rc files that I've made or found on the web that are useful.

Install
=======

There's a makefile that symbolically links the files here to the home directory.  You could copy them instead if you want to make changes to the local versions.

Sources
=======

+ https://github.com/mathiasbynens
+ http://www.twam.info/software/tune-terminal-in-os-x-lion
